import QtQuick 2.9
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.2
import Qt.labs.settings 1.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import UAdBlock 1.0

Page {
    id: settingsPage
    objectName: "settingsPage"
    anchors.fill: parent

    header: PageHeader {
        id: pageHeader
        title: i18n.tr("Settings")
        flickable: settingsFlickable
    }

    ScrollView {
        width: parent.width
        height: parent.height
        contentItem: settingsFlickable
    }

    Flickable {
        id: settingsFlickable
        width: parent.width
        height: parent.height
        contentHeight: mainColumn.height


        Column {
            id: mainColumn
            width: parent.width
            anchors.top: parent.top

            ListItem.SingleValue {
                text: i18n.tr("<b>General</b>")
            }
            ListItem.Standard {
                text: i18n.tr("Check for updates on app start?")
                control: Switch {
                    id: enablecheckUpdates
                    checked: settings.checkUpdates
                    onClicked: {
                        if(settings.checkUpdates)
                            settings.checkUpdates = false
                        else
                            settings.checkUpdates = true
                    }
                }
            }
            ListItem.Standard {
                text: i18n.tr("Enable push notifications?")
                control: Switch {
                    id: enablepushNotifications
                    checked: settings.pushNotifications
                    onClicked: {
                        if(settings.pushNotifications)
                            settings.pushNotifications = false
                        else
                            settings.pushNotifications = true
                    }
                }
            }

            ListItem.SingleValue {
                text: i18n.tr("<b>Security</b>")
            }
            ListItem.Standard {
                text: i18n.tr("Should the system remain in r/w status?")
                control: Switch {
                    id: enablesystemStatusRW
                    checked: settings.systemStatusRW
                    onClicked: {
                        if(settings.systemStatusRW)
                            settings.systemStatusRW = false
                        else
                            settings.systemStatusRW = true
                    }
                }
            }
            ListItem.SingleValue {
                text: i18n.tr("<b>Information</b>")
            }
            ListItem.SingleValue {
                text: i18n.tr("Version")
                value: "2.1.2"
            }
            ListItem.SingleValue {
                text: i18n.tr("Maintainer")
                value: "uAdBlock Team"
            }
            ListItem.SingleValue {
                text: i18n.tr("Push Token")
                value: pushClient.token
            }
        }
    }
}
